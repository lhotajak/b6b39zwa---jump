<?php
session_start();
include_once  "php/dbfunctions.php";
$user_logged = isset($_SESSION['user']);
$active = 0;
if (isset($_GET['active'])) { // get which tab is active
    $tmp = $_GET['active'];
    if ($tmp == 0 || $tmp == 1 || $tmp == 2) {
        $active = $tmp;
    }
}
$data = []; // get data according to an active tab
if ($active == 0) {
    $data = get_all_scores();
} elseif ($active == 1) {
    $data = get_scores_by_users();
} else {
    $data = get_scores_sums();
}
$bg_on = true;
if (isset($_COOKIE['skin'])) {
    $bg_on = $_COOKIE['skin'];
}
setcookie('skin', $bg_on, time() + 60 * 60 * 24 * 365, '/~lhotajak');
?>

<!DOCTYPE html>
<html lang="cs">

<head>
    <meta charset="UTF-8">
    <title>Jump!</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" title="style">
    <link rel="icon" href="images/icon.png">
    <style>
        /* special style for printing this leaderboard page -> only meaningful information */
        @media print {
            h1 {
                font-size: 32px;
            }

            h2 {
                font-size: 26px;
            }

            footer {
                display: none;
            }

            #upper_left_button {
                display: none;
            }

            #upper_right_content {
                display: none;
            }
        }

        <?php
        if ($bg_on) {
            echo 'body{background: linear-gradient(180deg, rgba(98, 122, 255, 1), rgba(66, 255, 88, 1) 76%) fixed;}';
        } else {
            echo 'body{background-color: rgb(98, 122, 255);}';
        }
        ?>
    </style>
</head>

<body>
    <h1>Jump!</h1>
    <footer>lhotajak | 2019</footer>

    <div id="content">
        <h2>
            Žebříček
        </h2>
        <div id="leaderboard_buttons">
            <a href="leaderboard.php?active=0" class="simple_button button_size <?php if ($active == 0) echo "active"; ?>">Všechny záznamy</a>
            <a href="leaderboard.php?active=1" class="simple_button button_size <?php if ($active == 1) echo "active"; ?>">Nejlepší výsledek hráče</a>
            <a href="leaderboard.php?active=2" class="simple_button button_size <?php if ($active == 2) echo "active"; ?>">Součet výsledků</a>
        </div>

        <p id="note">
            <?php
            switch ($active) {
                case 0:
                    echo 'Zde jsou vypsány všechny záznamy seřazené podle dosaženého skóre.';
                    break;
                case 1:
                    echo 'Zde jsou vypsány nejlepší výsledky jednotlivých hráčů. Seřazeno od nejlepšího';
                    break;
                case 2:
                    echo 'Zde jsou vypsány celkové součty dosaženého skóre jednotlivých hráčů.<br>Lze to interpretovat i jako počet odehraných milisekund.';
                    break;
                default:
                    break;
            }
            ?>
        </p>
        <div id="score_content">
            <?php
            // printing data
            foreach ($data as $key => $value) {
                echo $value;
            }
            ?>
        </div>
    </div>

    <a id="upper_left_button" class="simple_button" href="index.php">&lt; Zpět &gt;</a>

    <div id="upper_right_content">
        <!-- IF LOGGED -->
        <div id="urc_login_text" <?php if (!$user_logged) echo 'class="display_none"' ?>>
            Přihlášen jako <div class="strong inline"><?php echo $_SESSION['user'] ?></div>
            <div id="urc_logout">
                <a href="logout.php">Odhlásit</a>
            </div>

        </div>

        <!-- IF NOT LOGGED -->
        <div <?php if ($user_logged) echo 'class="display_none"' ?>>

            <!-- LOGIN FORM -->
            <form id="login_form" action="index.php" method="POST">
                <div class="login_line">
                    <label for="username_input">Uživatelské jméno:</label>
                    <input name="username" type="text" id="username_input" class="login_line_right" pattern="[A-Za-z0-9 ]*" required>
                </div>
                <div class="login_line">
                    <label for="password_input">Heslo:</label>
                    <input name="password" type="password" id="password_input" class="login_line_right" pattern="[A-Za-z0-9 ]*" required>
                </div>
                <div class="login_line">
                    <input value="Přihlásit" type="submit" id="login_submit">
                </div>
            </form>

            <div class="login_line">
                <a id="register" href="register.php">Ještě nemáš účet? Klikni sem.</a>
            </div>
        </div>

        <!-- LINK FOR CHANGING SKINS -->
        <div id="skinlink">
            <?php if ($bg_on) {
                echo '<a href="index.php?skin=0">';
                echo 'Změnit pozadí';
            } else {
                echo '<a href="index.php?skin=1">';
                echo 'Změnit pozadí';
            } ?>
            </a>
        </div>
    </div>
</body>

</html>