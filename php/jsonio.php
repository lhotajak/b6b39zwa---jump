<?php

/**
 * My IO functions for loading and writing $data in format of (associative) array into $filename
 */
function jsonio_write(string $filename, $data)
{
    $json_string = json_encode($data);
    file_put_contents($filename, $json_string);
}

function jsonio_load(string $filename)
{
    if (file_exists($filename)) {
        $json_string = file_get_contents($filename);
        return json_decode($json_string, true);
    } else {
        // if file doesn't exist then return empty array
        return [];
    }
}
