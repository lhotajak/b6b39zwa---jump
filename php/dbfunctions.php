<?php
include_once 'jsonio.php';
include_once 'hashfunction.php';

define("USERS", "data/users.mydb");
define("RECORDS", "data/records.mydb");

/**
 * Tries to log into the system with given username and password
 */
function try_log_in(string $username, string  $password)
{
    $users = jsonio_load(USERS);
    if (isset($users[$username])) {
        $hash = $users[$username];
        return myverify($password, $hash);
    }
    return false;
}

/**
 * Returns every record from db sorted by score
 */
function get_all_scores()
{
    $string_list = [];
    $data = jsonio_load(RECORDS);
    usort($data, function ($a, $b) {
        return $b[1] - $a[1];
    });
    foreach ($data as $key => $value) {
        // $value = [name, score, note]
        $idx = $key + 1;
        $s = '
        <div class="leaderboard_block">
            <div class="idx">' . $idx . '.</div> <div class="username">' . $value[0] . '</div> <div class="leaderboard_score">' . number_format($value[1], 0, ',', ' ') . '</div>
            <div class="note">' .  htmlspecialchars($value[2]) . '</div>
        </div>
        '; // note has to be put into htmlspecialchars, do not trust anybody...
        array_push($string_list, $s);
    }
    return $string_list;
}

/**
 * Returns best record from every user from db sorted by score
 */
function get_scores_by_users()
{
    // updates $player record with $score if it is 
    // better then that which is in $data 
    function update_data(&$data, $score, $player) // & needed to edit $data
    { /* PHP IS PAIN! */
        $updated = false;
        foreach ($data as $key => $value) {
            $pl = $value[0];
            if ($pl == $player) {
                $sc = $value[1];
                if ((int) $sc < (int) $score) {
                    $data[$key] = [$pl, $score];
                }
                $updated = true;
            }
        }
        if (!$updated) { // there is no record with player $player in $data yet
            array_push($data, [$player, $score]);
        }
    }
    $string_list = [];
    $all_data = jsonio_load(RECORDS);
    $data = []; // only the best scores
    // for each record in database filter the best for every user
    foreach ($all_data as $key => $value) {
        $s = $value[1];
        $p = $value[0];
        update_data($data, $s, $p);
    }
    usort($data, function ($a, $b) {
        return $b[1] - $a[1];
    });
    foreach ($data as $key => $value) {
        $idx = $key +  1;
        $s = '
        <div class="leaderboard_block">
            <div class="idx">' . $idx . '.</div> <div class="username">' . $value[0] . '</div> <div class="leaderboard_score">' . number_format($value[1], 0, ',', ' ') . '</div>
        </div>';
        array_push($string_list, $s);
    }
    return $string_list;
}


/**
 * Returns the sum of all scores for every player
 */
function get_scores_sums()
{
    $string_list = [];
    $all_data = jsonio_load(RECORDS); // all records
    $data = []; // sums for every user
    foreach ($all_data as $key => $value) {
        $username = $value[0];
        if (isset($data[$username])) {
            $val = $data[$username][1] + $value[1];
            $data[$username] =  [$username, $val, $data[$username][2] + 1];
        } else {
            $data[$username] =  [$username, $value[1], 1];
        }
    }
    usort($data, function ($a, $b) {
        return $b[1] - $a[1];
    });
    foreach ($data as $key => $value) {
        $idx = $key +  1;
        $s = '
        <div class="leaderboard_block">
            <div class="idx">' . $idx . '.</div> <div class="username">' . $value[0] . '</div> <div class="leaderboard_score">'
            . number_format($value[1], 0, ',', ' ') . '</div> <div class="count">Počet her: ' . $value[2] . '</div>
        </div>';
        array_push($string_list, $s);
    }
    return $string_list;
}

/**
 * adds user with given password to database (after hashing)
 */
function add_to_db(string $username, string  $password)
{
    $users = jsonio_load(USERS);
    $users[$username] = myhash($password);
    jsonio_write(USERS, $users);
}

/**
 * Returns true if username and password are valid strings, false otherwise
 */
function validate_register(string $username, string  $password)
{
    if (preg_match('/^[A-Za-z0-9 ]*$/', $username) && strlen($username) >= 3 && strlen($username) <= 16) {
        if (preg_match('/^[A-Za-z0-9 ]*$/', $password) && strlen($password) >= 4 && strlen($password) <= 16) {
            return true;
        }
    }
    return false;
}

/**
 * Adds record from game to database
 */
function add_record_to_db(string $user, string $score, string $note)
{
    $records = jsonio_load(RECORDS);
    array_push($records, [$user, $score, $note]);
    jsonio_write(RECORDS, $records);
}

/**
 * Returns true if user is database
 */
function user_in_db(string $user)
{
    $users = jsonio_load(USERS);
    return array_key_exists($user, $users);
}

/**
 * Deletes everything from every database text file
 */
function reset_database()
{
    jsonio_write(USERS, []);
    jsonio_write(RECORDS, []);
}