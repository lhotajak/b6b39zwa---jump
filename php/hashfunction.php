<?php
/**
 * In case of further developement I made a separate file for hashing and verifying passwords
 * But for now I use the php default
 */
function myhash(string $password)
{
    return password_hash($password, PASSWORD_DEFAULT);
}

function myverify(string $password, string $hash)
{
    return password_verify($password, $hash);
}
