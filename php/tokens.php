<?php
/**
 * Creates, stores and returns randomly generated token
 */
function create_token()
{
    // session_start(); -> session_start already called in index.php
    $token = rand();
    $_SESSION['token'] = $token;
    return $token;
}

/**
 * Checks token
 */
function check_token($token)
{
    session_start();
    if (isset($_SESSION['token'])) {
        return $_SESSION['token'] == $token;
    }
    return false;
}
