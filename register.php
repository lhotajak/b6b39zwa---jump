<?php
session_start();
include_once "php/dbfunctions.php";

$reg_error = "";
$user_logged = isset($_SESSION['user']);
if (isset($_POST['username']) && isset($_POST['password'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];
    if ($password == $_POST['password2']) {
        if (!validate_register($username, $password)) {
            // username or password is in wrong format
            // should not happen with JS turned on
            $reg_error = '
            <div class="reg_error">Chyba:</div>
            <div class="reg_error">Jméno nebo heslo je v chybném formátu</div>';
        } else {
            if (user_in_db($username)) {
                // username is already in db
                $reg_error = '
                <div class="reg_error">Chyba:</div>
                <div class="reg_error">Toto jméno je již obsazeno</div>';
            } else {
                // successful registration
                add_to_db($username, $password);
                $_SESSION['user'] = $username;
                header('Location: index.php');
            }
        }
    } else {
        // passwords don't match
        $reg_error = '
        <div class="reg_error">Chyba:</div>
        <div class="reg_error">Hesla se neshodují</div>';
    }
}
$bg_on = true;
if (isset($_COOKIE['skin'])) {
    $bg_on = $_COOKIE['skin'];
}
setcookie('skin', $bg_on, time() + 60 * 60 * 24 * 365, '/~lhotajak');
?>
<!DOCTYPE html>
<html lang="cs">

<head>
    <meta charset="UTF-8">
    <title>Jump!</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" title="style">
    <style>
        <?php
        if ($bg_on) {
            echo 'body{background: linear-gradient(180deg, rgba(98, 122, 255, 1), rgba(66, 255, 88, 1) 76%) fixed;}';
        } else {
            echo 'body{background-color: rgb(98, 122, 255);}';
        }
        ?>
    </style>
    <script src="js/register_validation.js"></script>
    <link rel="icon" href="images/icon.png">
</head>

<body>
    <h1>Jump!</h1>
    <footer>lhotajak | 2019</footer>

    <div id="content">
        <h2>
            Registrace
        </h2>

        <!-- REGISTRATION FORM -->
        <div id="reg_form_wrapper">
            <form id="reg_form" action="register.php" method="post">
                <div class="reg_line">
                    <label for="reg_name">Jméno: </label>
                    <input type="text" name="username" id="reg_name" class="text_box" pattern="[A-Za-z0-9 ]*" required>
                    <div class="register_note">
                        Jméno musí obsahovat alespoň 3 a maximálně 16 znaků [číslo, písmeno, mezera] - bez diakritiky!
                    </div>
                </div>
                <div class="reg_line">
                    <label for="reg_password">Heslo: </label>
                    <input type="password" name="password" id="reg_password" class="text_box" pattern="[A-Za-z0-9 ]*" required>
                    <div class="register_note">
                        Heslo musí obsahovat alespoň 4 a maximálně 16 znaků [číslo, písmeno, mezera] - bez diakritiky!
                    </div>
                </div>
                <div class="reg_line">
                    <label for="reg_password2">Heslo znovu: </label>
                    <input type="password" name="password2" id="reg_password2" class="text_box" pattern="[A-Za-z0-9 ]*" required>
                </div>
                <div>
                    <input id="reg_submit" type="submit" value="Registrovat" class="simple_button">
                </div>
            </form>
        </div>
        <div id="reg_errors">
            <?php
            echo $reg_error;
            ?>
        </div>
    </div>

    <div id="upper_right_content">
        <!-- IF LOGGED -->
        <div id="urc_login_text" <?php if (!$user_logged) echo 'class="display_none"' ?>>
            Přihlášen jako <div class="strong inline"><?php echo $_SESSION['user'] ?></div>
            <div id="urc_logout">
                <a href="logout.php">Odhlásit</a>
            </div>

        </div>

        <!-- IF NOT LOGGED -->
        <div <?php if ($user_logged) echo 'class="display_none"' ?>>

            <!-- LOGIN FORM -->
            <form id="login_form" action="index.php" method="POST">
                <div class="login_line">
                    <label for="username_input">Uživatelské jméno:</label>
                    <input name="username" type="text" id="username_input" class="login_line_right" pattern="[A-Za-z0-9 ]*" required>
                </div>
                <div class="login_line">
                    <label for="password_input">Heslo:</label>
                    <input name="password" type="password" id="password_input" class="login_line_right" pattern="[A-Za-z0-9 ]*" required>
                </div>
                <div class="login_line">
                    <input value="Přihlásit" type="submit" id="login_submit">
                </div>
            </form>

            <div class="login_line">
                <a id="register" href="register.php">Ještě nemáš účet? Klikni sem.</a>
            </div>
        </div>

        <!-- LINK FOR CHANGING SKINS -->
        <div id="skinlink">
            <?php if ($bg_on) {
                echo '<a href="index.php?skin=0">';
                echo 'Změnit pozadí';
            } else {
                echo '<a href="index.php?skin=1">';
                echo 'Změnit pozadí';
            } ?>
            </a>
        </div>
    </div>

    <a id="upper_left_button" class="simple_button" href="index.php">&lt; Zpět &gt;</a>
</body>

</html>