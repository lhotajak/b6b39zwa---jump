/**
 * Add JS validation for registration
 * */
function handleRegistration() {
    const user = document.getElementById("reg_name").value;
    const pass = document.getElementById("reg_password").value;
    const pass2 = document.getElementById("reg_password2").value;
    let errorMsg = ""; // error message 
    if (user.length < 3) {
        errorMsg += '<div class="reg_error">Jméno je příliš krátké! (min 3 znaky)</div>\n';
    } else if (user.length > 16) {
        errorMsg += '<div class="reg_error">Jméno je příliš dlouhé! (max 16 znaků)</div>\n';
    }
    const regex = /^[A-Za-z0-9 ]*$/;
    if (!regex.test(user)) {
        errorMsg += '<div class="reg_error">Jméno obsahuje neplatné znaky!</div>\n';
    }

    if (pass.length < 4) {
        errorMsg += '<div class="reg_error">Heslo je příliš krátké! (min 4 znaky)</div>\n';
    } else if (pass.length > 16) {
        errorMsg += '<div class="reg_error">Heslo je příliš dlouhé! (max 16 znaků)</div>\n';
    }

    if (!regex.test(pass)) {
        errorMsg += '<div class="reg_error">Heslo obsahuje neplatné znaky!</div>\n';
    }
    if (pass != pass2) {
        errorMsg += '<div class="reg_error">Kontrolní heslo není stejné!</div>\n';
    }
    if (errorMsg == "") {
        document.getElementById("reg_form").submit(); // submit the form
    } else {
        document.getElementById("reg_errors").innerHTML = '<div class="reg_error">Chyba:</div>\n' + errorMsg;
    }
}

// this function is called when page is loaded
function addValidationListener() {
    const submit = document.getElementById("reg_submit");

    function clickReaction(event) {
        event.preventDefault(); // prevent submiting
        handleRegistration();
    }
    submit.addEventListener("click", clickReaction, false);
}

window.addEventListener("load", addValidationListener, false);