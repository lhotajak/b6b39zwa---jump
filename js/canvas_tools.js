const CANVAS_WIDTH = 1600;
const CANVAS_HEIGHT = 540;
const CANVAS_ID = "game_canvas";


const SUN_SIZE_W = 150;
const SUN_SIZE_H = 150;
const SUN_TOP = 0;
const SUN_LEFT = 0;


/**
 * Creates the canvas for the game
 */
function createCanvas() {
    const canvasElem = document.createElement("canvas");
    canvasElem.setAttribute("id", CANVAS_ID);
    canvasElem.setAttribute("width", CANVAS_WIDTH);
    canvasElem.setAttribute("height", CANVAS_HEIGHT);
    const canvasWrapper = document.getElementById("canvas_wrapper");
    canvasWrapper.appendChild(canvasElem);
}


/**
 * Wrapper for canvas operations
 */
class CanvasTools {
    constructor(sprite_width, sprite_height) {
        createCanvas();
        this.canvas = document.getElementById(CANVAS_ID);
        this.canvasContext = this.canvas.getContext("2d");
        this.bg_img = document.getElementById("canvas_image_bg");
        this.sprite_img = document.getElementById("canvas_sprite");
        this.platform_img = document.getElementById("canvas_platform");
        this.sun_img = document.getElementById("canvas_sun");
        this.sprite_width = sprite_width;
        this.sprite_height = sprite_height;

        this.canvasContext.fillStyle = '#000000'; // color of obstacles (black)
        this.canvasContext.font = '32px Arial'; // size and font
    }

    /**
     * Draws background according to a given offset
     * @param {number} offset 
     */
    drawBackground(offset) {
        if (offset >= 1600 || offset < 0) {
            console.log("Error in CanvasTools.drawBackground: offset out of range.");
        }
        this.canvasContext.drawImage(this.bg_img, -offset, 0);
        this.canvasContext.drawImage(this.bg_img, 1600 - offset, 0);
    }

    /**
     * Draws sprite
     * @param {object} sprite 
     */
    drawSprite(sprite) {
        this.canvasContext.drawImage(this.sprite_img, sprite.x, sprite.y, this.sprite_width, this.sprite_height);
    }

    /**
     * Draws platform
     * @param {number} top 
     */
    drawPlatform(top) {
        this.canvasContext.drawImage(this.platform_img, 0, top);
    }

    /**
     * Draws sun
     */
    drawSun() {
        this.canvasContext.drawImage(this.sun_img, SUN_LEFT, SUN_TOP, SUN_SIZE_W, SUN_SIZE_H);
    }

    /**
     * Draws obstacles according to a given list of obstacles
     * @param {object} obstacles 
     */
    drawObstacles(obstacles) {
        for (let i = 0; i < obstacles.length; i++) {
            this._drawObstacle(obstacles[i]);
        }
    }

    _drawObstacle(obstacle) {
        this.canvasContext.fillRect(obstacle[0], obstacle[1], obstacle[2], obstacle[3]);
    }

    /**
     * Draws score on the canvas
     * @param {number} score 
     */
    drawScore(score) {
        const s = "Skóre: " + score.toLocaleString();
        this.canvasContext.fillText(s, CANVAS_WIDTH - 300, 75);
    }
}