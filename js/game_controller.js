// sprite size ratio -> image ratio should be the same (or at least similar)
const SPRITE_WIDTH = 4;
const SPRITE_HEIGHT = 5;

// size multiplier
const SPRITE_SIZE = 20;

// sprite distance from left (in % of canvas width)
const SPRITE_LEFT = 10;

// sprite distance from top (in % of canvas height)
// it also moves with the platform
const SPRITE_TOP = 70;

// number of obstacles on the screen
const N_OBSTACLES = 4;
const OBSTACLES_MAX_H = 32;
const AVG_OBSTACLE_W = 40;
const CHANCE_TO_MISS = 0.25; // chance to miss 1 obstacle

/**
 * Controls and "encapsulates" game entities
 */
class GameController {
    constructor(userLogged) {
        // GameController constants
        this.bgSpeed = 0.08; // 80 unites per second
        this.bgSpeedBeforePlaying = 0.25;
        this.obstacleSpeed = 0.3;
        this.maxObstacleSpeed = 0.50;

        this.GRAVITY_CONST = 0.004; // 0.004 units per millisecond^2
        this.INIT_JUMP_SPEED = 1.6; // 1.6 units per millisecond

        this.sprite_w = SPRITE_WIDTH * SPRITE_SIZE;
        this.sprite_h = SPRITE_HEIGHT * SPRITE_SIZE;
        this.canvasTools = new CanvasTools(this.sprite_w, this.sprite_h);
        this.sprite_left = this.canvasTools.canvas.width * SPRITE_LEFT / 100;
        this.sprite_top = this.canvasTools.canvas.height * SPRITE_TOP / 100;
        this.platform_top = this.sprite_top + this.sprite_h

        this.userLogged = userLogged;
    }

    /**
     * Initializes all the variables
     * Adds events listeners
     */
    init() {
        this._initStateVariables(); // creates state variables

        // run background animation before the actuall start of the game
        this.oldTimeStamp = 0;
        this.bgAnimId = window.requestAnimationFrame(this._bg_loop.bind(this));

        this._addButtonListeners();
    }

    _addButtonListeners() {
        // start the game by pressing spacebar
        function startFunction(event) {
            if (event.keyCode == 32) {
                event.preventDefault();
                document.body.removeEventListener("keydown", startFunction);
                thisObj._startGame();
            }
        }
        document.body.addEventListener("keydown", startFunction, false);


        // start the game by clicking the game_button_start button
        const thisObj = this;
        this.start_button = document.getElementById("game_button_start");
        this.start_button.addEventListener("click", e => {
            document.body.removeEventListener("keydown", startFunction);
            thisObj._startGame();
        }, false);

        // reset the game by clicking the reset_game button after the game is over
        const reset = document.getElementById("reset_game");
        reset.addEventListener("click", e => {
            location.reload(false); // reloads the current page from the cache
        }, false);
    }

    _startGame() {
        // remove button
        this.start_button.style.setProperty("display", "none");

        // call this._jumpCall() after pressing spacebar or touching the canvas_wrapper element with mobile device
        this.jumpingFunction = event => {
            if (event.keyCode == 32) {
                this._jumpCall();
                event.preventDefault();
            }
        }
        this.jumpingFunctionPhone = event => {
            this._jumpCall();
        }
        document.body.addEventListener("keydown", this.jumpingFunction, false);
        document.getElementById("canvas_wrapper").addEventListener("touchstart", this.jumpingFunctionPhone, false);

        window.cancelAnimationFrame(this.bgAnimId); // turn off the old bg animation
        window.requestAnimationFrame(this._gameLoop.bind(this)); // start the gameLoop animation
        this.startTime = Date.now(); // store the startTime for calculating the score
    }

    _initStateVariables() {
        // sprite.x should be the same during the whole runtime
        // sprite.y and sprite.ySpeed change when this._jumpCall() is called 
        this.sprite = { x: this.sprite_left, y: this.sprite_top, ySpeed: 0 };
        this.obstacles = this._generateObstacles();
        this.bgOffset = 0;
    }

    // generates the first N_OBSTACLES obstacles in the game
    // the first half of them have 0 height -> they are "empty"
    _generateObstacles() {
        // every obstacle is an array in format [x, y, width, height]
        const obstacles = [];
        const space = this.canvasTools.canvas.width / N_OBSTACLES;
        this.max_obstacle_height_px = this.canvasTools.canvas.height * OBSTACLES_MAX_H / 100;

        for (let i = 0; i < N_OBSTACLES; i++) {
            if (i < N_OBSTACLES / 2) {
                obstacles.push([i * space, 0, 0, 0]); // the first half is empty
            } else {
                // x := i*space
                // the rest is generated in _getNewObstacle
                obstacles.push(this._getNewObstacle(i * space));
            }
        }
        return obstacles;
    }

    _jumpCall() {
        if (sameFloat(this.sprite_top, this.sprite.y)) { // jump only if standing on platform
            // set the initial speed
            this.sprite.ySpeed = this.INIT_JUMP_SPEED;
        }
    }

    // the game loop that is called by window.requestAnimationFrame during the runtime of the application
    _gameLoop(timeStamp) {
        let millisPassed = timeStamp - this.oldTimeStamp;
        this.oldTimeStamp = timeStamp;

        if (millisPassed > 200) {
            // millisPassed is usually 16-17 (60 FPS), so if there is value bigger than let's say 200
            // it is certain, that user somehow stopped the game and tries to hack it
            alert("Bylo detekováno pozastavení hry!"); // alert user, that an iteruption of the game has been detected
            this._showGameOver();
            return;
        }
        this._update(millisPassed);
        this._draw();
        if (this._colisionDetected()) {
            this._showGameOver(false);
        } else {
            window.requestAnimationFrame(this._gameLoop.bind(this));
        }
    }

    // updates all the states and variables in the game depending on how many milliseconds have passed since the last call
    _update(millisPassed) {
        // update background offset
        this.bgOffset += millisPassed * this.bgSpeed;
        if (this.bgOffset >= 1600) {
            this.bgOffset %= 1600;
        }

        // update obstacles position
        this._updateObstacles(millisPassed);

        // if the sprite is on the ground or has non-zero speed then update its state
        if (!sameFloat(this.sprite.y, this.sprite_top) || !sameFloat(this.sprite.ySpeed, 0.0)) {
            this._updateSpriteState(millisPassed);
        }

        // make obstacle move faster until they reach max speed
        if (this.obstacleSpeed < this.maxObstacleSpeed) {
            // I know that adding significantlly smaller values to IEEE-754 float is a crime, but it works
            this.obstacleSpeed += millisPassed / 300000;
        }

        this.score = Date.now() - this.startTime; // calculating the current score
    }

    _updateSpriteState(millisPassed) {
        // called in _update when the sprite is moving verticaly
        // compute sprite.y and sprite.ySpeed
        // https://en.wikipedia.org/wiki/Free_fall#Free_fall_in_Newtonian_mechanics
        let nextY = this.sprite.y - (this.sprite.ySpeed * millisPassed - (this.GRAVITY_CONST * millisPassed ** 2) / 2);
        this.sprite.y = Math.min(this.sprite_top, nextY); // sprite can't "fall into the platform"
        this.sprite.ySpeed = this.sprite.ySpeed - this.GRAVITY_CONST * millisPassed;
    }

    _updateObstacles(millisPassed) {
        const d = millisPassed * this.obstacleSpeed
        for (let i = 0; i < this.obstacles.length; i++) {
            const element = this.obstacles[i];
            element[0] -= d; // shift every obstacle by the same amount of distance
        }

        const firstObstacle = this.obstacles[0];
        // create new obstacle
        if (this.obstacles.length == N_OBSTACLES && firstObstacle[0] < -firstObstacle[2] / 2) {
            // x := the right edge of canvas - half the width of the deleted obstacle
            this.obstacles.push(this._getNewObstacle(CANVAS_WIDTH));
        }
        // deleting the first obstacle
        if (firstObstacle[0] < -firstObstacle[2]) { // obstacle can't be seen anymore 
            this.obstacles.shift();
        }
    }

    _getNewObstacle(x) {
        if (Math.random() < CHANCE_TO_MISS) { // chance to miss one obstacle
            return [x, 0, 0, 0];
        }

        // width := avg_width +- 0.5 * avg_width
        const width = AVG_OBSTACLE_W + ((2 * Math.random() - 1) * 0.5 * AVG_OBSTACLE_W)
        const r = (Math.random() + 0.5) / 1.5; // coefficient of obstacle height

        // left, top, width, height
        return [x - width / 2, this.platform_top - r * this.max_obstacle_height_px, width, 500];
    }

    // returns true iff a colision with any obstacle was detected
    _colisionDetected() {
        for (let i = 0; i < N_OBSTACLES; i++) {
            // I'm checking every obstacle even though it's not really needed
            if (this._colisionWithSprite(this.obstacles[i])) {
                return true;
            }
        }
        return false;
    }

    _colisionWithSprite(obstacle) {
        function rectIntersect(x1, y1, w1, h1, x2, y2, w2, h2) {
            if (x2 > w1 + x1 || x1 > w2 + x2 || y2 > h1 + y1 || y1 > h2 + y2) {
                return false;
            }
            return true;
        }
        const o = obstacle;
        const c = 5; // amount of units (pixels) removed from the sprite from each side -> better feeling when colision happens
        return rectIntersect(this.sprite_left + c, this.sprite.y + c, this.sprite_w - 2 * c, this.sprite_h - 2 * c, o[0], o[1], o[2], o[3]);
    }

    // draws all the entities on tha canvas using canvasTools
    _draw() {
        this.canvasTools.drawBackground(this.bgOffset);
        this.canvasTools.drawSprite(this.sprite);
        this.canvasTools.drawSun();
        this.canvasTools.drawObstacles(this.obstacles); // must be drawn before platform
        this.canvasTools.drawPlatform(this.platform_top);
        this.canvasTools.drawScore(this.score);
    }

    // just a background loop running in the background before the game starts
    _bg_loop(timeStamp) {
        this.bgOffset += (timeStamp - this.oldTimeStamp) * this.bgSpeedBeforePlaying;
        this.oldTimeStamp = timeStamp;
        if (this.bgOffset >= 1600) {
            this.bgOffset %= 1600;
        }
        this.canvasTools.drawBackground(this.bgOffset);
        this.canvasTools.drawSun();
        this.bgAnimId = window.requestAnimationFrame(this._bg_loop.bind(this));
    }

    // shows the GAME OVER message and form (when user is logged into the app)
    _showGameOver(stopDetected) {
        const gameOver = document.getElementById("game_over_content");
        gameOver.style.setProperty("display", "block");
        const scoreEl = document.getElementById("score");
        scoreEl.innerText = this.score.toLocaleString();

        if (this.userLogged) {
            // set score in a hidden element - with hash
            document.getElementById("hidden_score").value = String(this.score) + " " + getHashedScore(this.score);
        }

        // remove the event listener so user can use spacebar again
        document.body.removeEventListener("keydown", this.jumpingFunction);
    }
}

/* ################### */

// Helper functions
/**
 * Returns true if f1 and f2 are same or very similar numbers (floats)
 * @param {number} f1 
 * @param {number} f2 
 */
function sameFloat(f1, f2) {
    return Math.abs(f1 - f2) < 0.000001;
}

/**
 * Returns the first half of reversed md5 hex digest
 * I'm using it for validation of records send by POST method on server
 * @param {number} score
 */
function getHashedScore(score) {
    score += 13 // for more security
    const s = String(score); // number to string
    const md5score = hex_md5(s); // hex string
    const reversed = md5score.split("").reverse().join(""); // reversed string
    return reversed.substring(0, 16); // first 16 chars
}