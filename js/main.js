/**
 * Entry point of the script
 */
function main() {
    // removig element that should not be in page when JS is enabled
    document.getElementById("no_js_text").style.setProperty("display", "none");

    // is user logged into the app?
    const userLogged = parseInt(document.getElementById("user_logged").innerText) == 1;

    // display start button
    document.getElementById("game_button_start").style.setProperty("display", "block");

    // create the game
    const gameObject = new GameController(userLogged);
    gameObject.init();
}
window.addEventListener("load", main, false);