# Jump!
### Jakub Lhoták
##### ČVUT FEL - B6B39ZWA
#### [http://wa.toad.cz/~lhotajak/Jump!/](http://wa.toad.cz/~lhotajak/Jump!/)
Cílem tohoto projektu bylo vytvoření webové aplikace pro předmět B6B39ZWA (Základy webových aplikací).  Téma jsme si mohli vybrat libovolné. Jediné, co jsme museli dodržet, bylo splnění všech povinných bodů v [této hodnotící tabulce](https://docs.google.com/spreadsheets/d/15wmEuTG90pCtIcMwcXFSweAoFE6xA4QCrM20Ov7NUa4/edit#gid=2084582211). Jelikož rád programuji originální a interaktivní programy, rozhodl jsem se, že zkusím naprogramovat jednoduchou hru v čistém JavaScriptu s využitím HTML5 Canvasu.

### Popis úlohy - zadání
> Formální zadání tak, jako by bylo od externího zadavatele. Zadání bude
> obsahovat i akceptační podmínky.

Úkolem je vytvořit **browser hru**, kde cílem hry je přeskočit přes co největší počet náhodně vygenerovaných překážek za nějakou figurku. Čím déle se hráči daří nenarazit do překážky, tím větší je jeho skóre. Hráči se mohou do systému **registrovat a přihlásit**. **Přihlášený** hráč může **přidat své skóre do žebříčku** a soutěžit tak s ostatními hráči.
#### Akceptační Kriteria
- Hra musí být hratelná a funkční
- Do systému se lze registrovat a přihlásit
- Lze přidávat skóre do žebříčku
- Systém musí být odolný proti hackerům/zlým uživatelům

### Uživatelská příručka
#### Hra
Po otevření vstupní stránky systému (index.php) uvidíte uprostřed velké okno (dále jen canvas), na kterém běží animace pozadí.  Uprostřed canvasu se nachází tlačítko, po jehož kliknutí se hra spustí. Hru lze spustit i zmáčknutím mezerníku. Jakmile hru spustíte, tak animace pozadí se zpomalí a v levé části canvasu se vytvoří malá černá figurka. Každým zmáčknutím mezerníku figurka vyskočí. Cílem hry je přeskočit co nejvíce překážek (černých obdélníků), které se zprava blíží. Když se Vám nepodaří přeskočit překážku a narazíte do ní, tak hra končí. Po skončení hry se zobrazí Vaše skóre a pod ním tlačítko 'Hrát znovu', které hru restartuje a můžete hrát znovu. 
#### Registrace a přihlášení
K registračnímu formuláři se ze vstupní stránky prokliknete odkazem v pravé horní části stránky ("Ještě nemáš účet? Klikni sem."). Po kliknutí Vás tento odkaz přesměruje na samostatnou stránku registrace. Zde jen vyplníte údaje (jméno, heslo) a potvrdíte formulář stiskem tlačítka 'Registrovat'. Pokud registrace proběhla úspěšně, dostanete se opět na úvodní stránku, nyní už jako přihlášený uživatel. Pokud byste se chtěl odhlasit, použijete k tomu odkaz 'Odhlásit' v pravém horním rohu. Přihlášení se provede zadáním přezdívky a hesla do políček v pravém horním rohu (aby se tento formulář objevoval, tak uživatel musí být odhlášený). Jako přihlášený uživatel můžete přidat své skóre do žebříčku. Uděláte to odesláním formuláře, který se Vám vygeneruje poté, co narazíte do překážky. V tomto formuláři můžete (je to volitelné) vyplnit textové pole s poznámkou. Po odeslání formuláře Vás systém přesměruje na stránku se žebříčkem, kde uvidíte Vaše skóre.
#### Ostatní funkce
##### Skin
Systém umožňuje změnit vzhled pozadí stránek. Přepíná se zde mezi dvěma skiny pomocí malého odkazu 'Změnit pozadí' v pravém horním rohu. Systém si pamatuje, jaké pozadí jste naposledy zvolil, což znamená, že při opakované návštěvě stránek Vám zůstane Vámi vybrané pozadí.
##### Žebříček
Systém Vám umožňuje prohlédnout si nejlepší dosažené výsledky uložené v databázi. Z úvodní stránky se na stráknu s žebříčkem dostanete kliknutím na tlačítko 'Žebříček' v levé horní části stránky. Žebříček má celkem 3 módy: *Všechny záznamy*, *Nejlepší výsledek hráče* a *Součet výsledků*. Každý mód zobrazuje jinou statistiku a lze mezi nimi jednoduše přepínat tlačítky v horní části stránky. Výsledky jsou vždy seřazené sestupně, tedy nejlepší záznam/hráč má vždy číslo 1.

### Ukázky UI
Výstřižek skákající figurky z hlavní stránky:
![game canvas with jumping sprite](https://gitlab.fel.cvut.cz/lhotajak/b6b39zwa---jump/raw/master/screenshots/canvas.png)

Formulář s registrací:
![registration](https://gitlab.fel.cvut.cz/lhotajak/b6b39zwa---jump/raw/master/screenshots/reg.PNG)

Přihlašovací formulář (+ odkaz na změnu skinu):
![login form](https://gitlab.fel.cvut.cz/lhotajak/b6b39zwa---jump/raw/master/screenshots/login.png)

Žebříček, kde je vybraná 3. záložka (Součet výsledků)
![leaderboard](https://gitlab.fel.cvut.cz/lhotajak/b6b39zwa---jump/raw/master/screenshots/leaderboard.png)
### Popis implementace
##### Klient
Klientská část aplikace je psána v HTML5, CSS a JavaScriptu. Systém je rozdělen do několika stránek, mezi kterými si uživatel podle potřeby přepíná.
Na klientské části aplikace používám různé zajímavější techniky a přístupy. Za zmínku stojí praktická a spolehlivá kontrola uživatelských vstupů při registraci pomocí JavaScriptu (použití regulárních výrazů, různé chybové hlášky). Tou největší zajímavostí je ovšem mnou navržený herní engine napsaný v čistém JavaScriptu. Jako inspiraci jsem použil [tento](https://spicyyoghurt.com/tutorials/html5-javascript-game-development/setup-html5-canvas-game) článek, ve kterém je moc hezky vysvětleno, jak vytvořit herní smyčku v JS pomocí funkce 
*window.requestAnimationFrame*.
Dále jsem v souboru leaderboard.php (tedy Žebříček) implementoval jednoduchý styl pro tisk dokumentu (@media  print {...}). Styl pro tisk má pouze tato stránka, protože pro zbylé stránky to nemá cenu implementovat.
##### Server
Server je psaný v PHP a přístup k databázi simuluji pomocí čtení/zapisování do textových souborů. Data do souborů zapisuji ve formátu JSON. 
Serverová část aplikace už není tak bohatá na zajímavosti jako část klientská, ale i tak se tam najde několik technik, které stojí za zmínku. Tou nejzajímavější bude nejspíše použití CSRF tokenu při komunikaci s klientem. Tuto techniku používám proto, abych zabránil odeslání stejného POST requestu vícekrát. Kdybych tuto ochranu nepoužil, tak by bylo možné databázi "zaspamovat" stejnými výsledky z her se stejným skóre (útočník by posílal jeden a ten samý POST request).
##### Bezpečnost
V této aplikaci jsem velmi dbal na to, aby nebylo nijak možné podvádět, nebo provádět nepovolené akce. To je ovšem ve světě webových aplikací docela netriviální úloha...
Použil jsem různé techniky, proti různým druhům útoků:
- používám funkci *htmlspecialchars* pro vypisování uživatelských výstupů
- *CSRF tokeny* pro zamezení odesílání stejných POST requestů
- všechny hesla jsou zahashovaná pomocí PHP funkce *password_hash;*
- při odesílání výsledku hry z klienta (skóre) na sever klient před odesláním hashuje hodnotu *$_POST['score']* pomocí mého upraveného md5 algoritmu - ten samý algoritmus je potom použit na serveru a tím si kontroluji, že POST request byl opravdu odeslaný z mého klienta a nejedná se o útok

Aby čtvrtý krok měl nějaký smysl, tak se musí nejdříve JavaScriptový kód dobře "zamaskovat". K tomu jsem pro finální public verzi aplikace (která je nyní na školním serveru) použil tyto dvě služby - [Closure Compiler](https://closure-compiler.appspot.com/home) a [JS Obfuscate](https://www.cleancss.com/javascript-obfuscate/index.php), které mi výsledný JavaScriptový kód převedly do nečitelné podoby a tudíž téměř nikdo není schopný zjistit, jakou hashovací funkci používám.
**EDIT:** Na výsledný JS kód jsem ještě použil službu [JScrewIt](https://jscrew.it/), která převede libovolný JS kód do podoby, které se říká [JSFuck](https://en.wikipedia.org/wiki/JSFuck).

### Poznámka
Tvorba této aplikace mi zabrala o dost více času, než jsem tomu původně chtěl věnovat. Nicméně nemyslím si, že to byl promrhaný čas. Práce na tomto projektu mě bavila a vyzkoušel jsem si na ní, jak fungují reálné aplikace tohoto typu.
Jump! teď rozhodně není ve stavu který bych nazval *finalní*. Detekce kolizí by šla hodně zlepšit, figurka by mohla mít nějakou animaci pohybu, překážky by mohly být o hodně zajímavější než jenom černé obdélníky, atd... Těch zlepšení je opravdu mnoho, ale je to už všechno více méně nad rámec ZWA semestálky a rozhodně na to teď nemám čas.

Možná se ale k tomu ještě někdy vrátím a dotáhnu to do konce...
:)

