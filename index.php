<?php
session_start();
include_once "php/dbfunctions.php";
include_once "php/tokens.php";

$login_failed = false;
if (isset($_POST['username']) && isset($_POST['password'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];
    if (try_log_in($username, $password)) { // trying to log in with given username and password
        $_SESSION['user'] = $username;
    } else {
        $login_failed = true;
    }
}
$user_logged = isset($_SESSION['user']);
$bg_on = true; // gradient background
if (isset($_COOKIE['skin'])) {
    $bg_on = $_COOKIE['skin']; // get skin
}
if (isset($_GET['skin'])) {
    $bg_on = $_GET['skin']; // get skin form GET method
}
setcookie('skin', $bg_on, time() + 60 * 60 * 24 * 365, '/~lhotajak'); // set skin
$token = create_token(); // create CSRF token
?>
<!DOCTYPE html>
<html lang="cs">

<head>
    <meta charset="UTF-8">
    <title>Jump!</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" title="style">
    <link rel="icon" href="images/icon.png">
    <style>
        /* body background according to which skin is set */
        <?php
        if ($bg_on) {
            echo 'body{background: linear-gradient(180deg, rgba(98, 122, 255, 1), rgba(66, 255, 88, 1) 76%) fixed;}';
        } else {
            echo 'body{background-color: rgb(98, 122, 255);}';
        }
        ?>
    </style>
    <script src="js/md5.js"></script>
    <script src="js/canvas_tools.js"></script>
    <script src="js/game_controller.js"></script>
    <script src="js/main.js"></script>
</head>

<body>
    <h1>Jump!</h1>
    <!-- CANVAS WRAPPER -->
    <div id="canvas_wrapper">

        <div id="no_js_text">
            Tato aplikace potřebuje mít povolený JavaScript ve Vašem prohlížeči.
        </div>

        <!-- START BUTTON -->
        <div id="game_button_start" class="simple_button">
            Klikni sem nebo zmáčkni mezerník
        </div>

        <!-- GAME OVER FORM -->
        <div id="game_over_content" class="center">

            <h3>GAME OVER</h3>
            <div id="score"></div>

            <!-- USER NOT LOGGED -->
            <form <?php if (!$user_logged) echo 'class="display_none"' ?> id="game_over_form" action="addscore.php" method="POST">
                <div class="display_none">
                    <input id="token" type="text" name="token" value=<?php echo '"'.$token.'"'?>>
                    <!-- will be filled with JS -->
                    <input id="hidden_score" type="text" name="score">
                    <input type="text" name="user" <?php if ($user_logged) echo "value=\"" . $_SESSION['user']. "\""; ?>>
                </div>
                <div>
                    <label for="score_note" class="block">Přidat poznámku (volitelné, max 70 znaků):</label>
                    <input type="text" name="note" id="score_note" maxlength="70">
                </div>
                <input value="Odeslat skóre" id="sumbit_score" class="simple_button" type="submit">
            </form>

            <!-- USER LOGGED -->
            <div <?php if ($user_logged) echo 'class="display_none"' ?> id="not_registered_text">Aby jsi mohl přidat své skóre do databáze, musíš být přihlášen.</div>
            <div class="simple_button" id="reset_game">Hrát znovu</div>
        </div>

        <!-- canvas will be created here with JS -->
    </div>

    <div id="content">
        <article>
            <h2>
                Jump!
            </h2>
            <h4>O hře</h4>
            <p>
                Tato hra vznikla jako semestrální projekt v předmětu ZWA.<br>
                Využívá pouze HTML5, CSS, čistý JS a PHP.
            </p>
            <h4>Jak hrát?</h4>
            <p>
                K hraní (skákání) se používá pouze klávesa mezerník. <br>
                Pokud používate mobil či tablet, můžete skákat dotykem na herní plochu. <br>
                Hru není možné pauznout! Jakýkoli pokus o pozastavení hry (ať už přímo z JS konzole, Google Chrome debuggeru, nebo třeba změnou záložky v prohlížeči) se bere jako podvádění a hra v tento moment končí.
            </p>
        </article>
    </div>

    <!-- LEADERBOARD BUTTON -->
    <a id="upper_left_button" class="simple_button" href="leaderboard.php">Žebříček</a>

    
    <div id="upper_right_content">
        <!-- IF LOGGED -->
        <div id="urc_login_text" <?php if (!$user_logged) echo 'class="display_none"' ?>>
            Přihlášen jako <div class="strong inline"><?php echo $_SESSION['user'] ?></div>
            <div id="urc_logout">
                <a href="logout.php">Odhlásit</a>
            </div>

        </div>

        <!-- IF NOT LOGGED -->
        <div <?php if ($user_logged) echo 'class="display_none"' ?>>

            <!-- LOGIN FORM -->
            <form id="login_form" action="index.php" method="POST">
                <div class="login_line">
                    <label for="username_input">Uživatelské jméno:</label>
                    <input name="username" type="text" id="username_input" class="login_line_right" pattern="[A-Za-z0-9 ]*" required>
                </div>
                <div class="login_line">
                    <label for="password_input">Heslo:</label>
                    <input name="password" type="password" id="password_input" class="login_line_right" pattern="[A-Za-z0-9 ]*" required>
                </div>
                <div class="login_line">
                    <input value="Přihlásit" type="submit" id="login_submit">
                </div>
            </form>

            <div class="login_line">
                <a id="register" href="register.php">Ještě nemáš účet? Klikni sem.</a>
            </div>
        </div>

        <!-- LINK FOR CHANGING SKINS -->
        <div id="skinlink">
            <?php if ($bg_on) {
                echo '<a href="index.php?skin=0">';
                echo 'Změnit pozadí';
            } else {
                echo '<a href="index.php?skin=1">';
                echo 'Změnit pozadí';
            } ?>
            </a>
        </div>
    </div>
    <?php
    if ($login_failed) {
        echo '<div id="error_login">Nepodařilo se přihlásit! Tato kombinace jména a hesla není známá.</div>';
    }
    ?>

    <footer>lhotajak | 2019</footer>

    <!-- for JS - user logged or not -->
    <div id="user_logged">
        <?php if ($user_logged) {
            echo '1';
        } else {
            echo '0';
        } ?>
    </div>

    <!-- image data for canvas  -->
    <img id="canvas_image_bg" class="display_none" src="images/bg_base.png" alt="baground hills">
    <img id="canvas_sprite" class="display_none" src="images/sprite.png" alt="jumping sprite">
    <img id="canvas_platform" class="display_none" src="images/platform.png" alt="platform">
    <img id="canvas_sun" class="display_none" src="images/sun.png" alt="Teletubbies sun">
</body>

</html>