<?php
include_once 'php/dbfunctions.php';
include_once "php/tokens.php";

// Validation
if (isset($_POST['user']) && isset($_POST['score']) && isset($_POST['note']) && isset($_POST['token'])) {
    $user = $_POST['user'];
    $score = $_POST['score'];
    $note = $_POST['note'];
    if (is_valid($user, $score, $note) && check_token($_POST['token'])) {
        add_record_to_db($user, explode(" ", $score)[0], $note); // explode(" ", $score)[0] is the score [1] is hash
        header('Location: leaderboard.php');
    } else {
        // this only happens when an attacker sends non-valid information
        header('Location: leaderboard.php');
    }
}

/**
 * Returns the first half of reversed md5 hex digest
 */
function getHashedScore(int $score)
{
    $score += 13; // for more security
    $s = strval($score); // number to string
    $md5score = md5($s); // hex string
    $reversed = strrev($md5score); // reversed string
    return substr($reversed, 0, 16); // first 16 chars
}

/**
 * Validates score and tries whether the computed and received hash from POST method are the same
 */
function validate_score_and_hash(string $score)
{
    $splitted = explode(" ", $score);
    if (count($splitted) == 2) {
        $s = $splitted[0];
        $hash = $splitted[1];
        // intval($s) returns 0 if $s is not a number, otherwise the actuall number as int
        $i = intval($s);
        if ($i > 0) {
            if (getHashedScore($i) == $hash) {
                return true;
            }
        }
    }
    return false;
}

// validates POST variables
function is_valid($user, $score, $note)
{    
    if (validate_score_and_hash($score) && $note <= 70 && user_in_db($user)) {
        return true;
    }
    return false;
}